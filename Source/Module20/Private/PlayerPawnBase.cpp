// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"
// Need "EnhancedInput" in ProjectName.Build.cs
// And reGenerate Visual Studio Project Files (delete "Binaries", "Intermediate", "Saved". Then right-click .uproject -> "Generate...")
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	
	CreateSnakeActor();

	// Enhanced Input Controller thing
	if (APlayerController* PlayerController = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(InputMapping, 0);
		}
	}
}

void APlayerPawnBase::MoveVertical(const FInputActionValue& Value)
{
	/*if (const float CurrentValue = Value.Get<float>())
	{
		UE_LOG(LogTemp, Warning, TEXT("IA_VerticalMove triggered"));
	}*/

	if (IsValid(SnakeActor))
	{
		if (Value.Get<float>() > 0 && SnakeActor->LastMovementDirection != EMovementDirection::DOWN)
		{
			SnakeActor->CurrentMovementDirection = EMovementDirection::UP;
		}
		if (Value.Get<float>() < 0 && SnakeActor->LastMovementDirection != EMovementDirection::UP)
		{
			SnakeActor->CurrentMovementDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::MoveHorizontal(const FInputActionValue& Value)
{
	/*if (const float CurrentValue = Value.Get<float>())
	{
		UE_LOG(LogTemp, Warning, TEXT("IA_HorizontallMove triggered"));
	}*/

	if (IsValid(SnakeActor))
	{
		if (Value.Get<float>() > 0 && SnakeActor->LastMovementDirection != EMovementDirection::LEFT)
		{
			SnakeActor->CurrentMovementDirection = EMovementDirection::RIGHT;
		}
		if (Value.Get<float>() < 0 && SnakeActor->LastMovementDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->CurrentMovementDirection = EMovementDirection::LEFT;
		}
	}
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Enhanced Input Controller thing
	if (UEnhancedInputComponent* EIC = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EIC->BindAction(VerticalMoveAction, ETriggerEvent::Triggered, this, &APlayerPawnBase::MoveVertical);
		EIC->BindAction(HorizontalMoveAction, ETriggerEvent::Triggered, this, &APlayerPawnBase::MoveHorizontal);
	}

	/*{
		if (ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player))
		{
			if (UEnhancedInputLocalPlayerSubsystem* InputSystem = LocalPlayer->GetSubsystem<UEnhancedInputLocalPlayerSubsystem>())
			{
				if (!InputMapping.IsNull())
				{
					InputSystem->AddMappingContext(InputMapping.LoadSynchronous(), Priority);
				}
			}
		}
	}*/
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

