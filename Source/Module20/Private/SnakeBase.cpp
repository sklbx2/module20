// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementPadding = 130.f;
	MovementSpeed = 0.5f;
	LastMovementDirection = EMovementDirection::DOWN;
	CurrentMovementDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		// i �� ��������� � ���������, ������ ��� �� ���������� ���������� ��������� �������,
		// ������� �� ����������� � ������ �������� ����� � ������� Add.
		// ��������, �������� ���������� ��������� � ������� ���������, ��� ������ ������������ i,
		// �� ��� ���� � �������, ����� ��� � ����� �����
		FVector NewPadding(SnakeElementArray.Num() * ElementPadding, 0, 0);
		FTransform NewLocation(NewPadding);
		ASnakeElementBase* SnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewLocation);
		//SnakeElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		
		int32 ElemIndex = SnakeElementArray.Add(SnakeElement);
		// ����� ��, � ������, ��� ����� ���� �� ������������ i
		if (i == 0)
		{
			SnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (CurrentMovementDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementPadding;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementPadding;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementPadding;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementPadding;
		break;
	default:
		break;
	}

	LastMovementDirection = CurrentMovementDirection;

	//AddActorWorldOffset(MovementVector);
	for (int i = SnakeElementArray.Num()-1; i > 0; i--)
	{
		auto CurrentElement = SnakeElementArray[i];
		auto PrevElement = SnakeElementArray[i - 1];
		CurrentElement->SetActorLocation(PrevElement->GetActorLocation());
	}
	
	SnakeElementArray[0]->AddActorWorldOffset(MovementVector);
}

